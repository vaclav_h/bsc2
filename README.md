# BSC2

Koukni na kiwi.com (možná znáš, je to český kdysi start-up). Jedná se o vyhledávač levných letenek, který ovšem taky poskytuje své veřejné HTTP API.
Napiš jednoduchou (konzolovou) aplikaci v Javě 8+ za použití Mavenu/Gradle pro build, která na vstupu (argumenty příkazové řádky) přijme 4 parametry:
- odkud letět (město)
- kam letět (město)
- odkdy letět (datum)
- dokdy letět (datum)
a s nimi provolá Kiwi API a na standardní (konzolový) výstup vypíše nalezený let.

Nemá to žádný časový limit, udělej to tak, aby to za tebe bylo v "rozumné" kvalitě (co je rozumné záleží na tvé posouzení). Platí, že mě můžeš kdykoliv v průběhu implementace kontaktovat a já se pokusím v dohledné době odpovědět (případně si můžeme klidně i zavolat).

Formu odevzdání nechám na tobě, ale moje preference je nějaké repo, kde bude i alespoň stručný popis, jak aplikaci správně sestavit a použít.

