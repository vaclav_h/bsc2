package cz.vhausler.controller;

import cz.vhausler.model.Data;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TravelControllerTests {

    TravelController instance;

    @Before
    public void init() {
        instance = new TravelController();
    }

    /*
        dates should probably be always in the future so calendar + some date magic could be used here
     */

    @Test
    public void getResultNullTest() {
        Data result = instance.getResult(null, null, null, null);
        assertNull(result);
    }

    @Test
    public void getResultNullTest2() {
        Data result = instance.getResult("CZ", " DE", " 01/06/2019", " 01/07/2019");
        assertNull(result);
    }

    @Test
    public void getResultTest() {
        Data result = instance.getResult("CZ", "DE", "01/06/2019", "01/07/2019");
        System.out.println(result);
        assertNotNull(result);
    }

    @Test
    public void getResultLongerRouteTest() {
        Data result = instance.getResult("CZ", "US", "01/06/2019", "01/07/2019");
        System.out.println(result);
        assertNotNull(result);
    }

    @Test
    public void isValidDateFormatFailTest() {
        String date = "3. 5. 1954";
        boolean result = instance.isValidDateFormat(date, TravelController.DEFAULT_DATE_FORMAT);
        assertFalse(result);
    }


    @Test
    public void isValidDateFormatTest() {
        String date = "02/05/2015";
        boolean result = instance.isValidDateFormat(date, TravelController.DEFAULT_DATE_FORMAT);
        assertTrue(result);
    }
}
