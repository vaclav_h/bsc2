package cz.vhausler.model;

public class Route {
    private String cityTo;

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }
}
