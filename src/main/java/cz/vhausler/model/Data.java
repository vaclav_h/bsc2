package cz.vhausler.model;

public class Data {
    private String id;
    private int price;
    private Route[] route;
    private String currency;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Route[] getRoute() {
        return route;
    }

    public void setRoute(Route[] route) {
        this.route = route;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (id.contains("|")) {
            sb.append("Flight ids: ");
        } else {
            sb.append("Flight id: ");
        }
        sb.append(id).append(", price: ").append(price).append(" ").append(currency);
        if (route != null && route.length > 1) {
            sb.append(", route: ");
            int i = 1;
            for (Route r : route) {
                sb.append(r.getCityTo());
                if (i != route.length) {
                    sb.append(", ");
                }
                i++;
            }
        }
        return sb.toString();
    }
}
