package cz.vhausler.controller;

import com.google.gson.Gson;
import cz.vhausler.model.Data;
import cz.vhausler.model.TravelResult;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class TravelController {

    // use return_from and return_to for return flights
    private String URL_BASE = "https://api.skypicker.com/flights?fly_from=%s&fly_to=%s&date_from=%s&date_to=%s&vehicle_type=aircraft";
    static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("dd/MM/YYYY");

    /**
     * Returns the cheapest flight for the specified parameters.
     *
     * @param from        departure location
     * @param to          arrival destination
     * @param dateFromStr date from
     * @param dateToStr   date to
     * @return flight info or null if the input arguments are incorrect
     */
    Data getResult(String from, String to, String dateFromStr, String dateToStr) {
        if (from == null || to == null || dateFromStr == null || dateToStr == null) {
            printUsage();
            return null;
        }

        // check if the dates have valid format
        // optional validation could be done to check if the dates are not in the past and that dateFrom is before dateTo
        if (!isValidDateFormat(dateFromStr, DEFAULT_DATE_FORMAT) || !isValidDateFormat(dateToStr, DEFAULT_DATE_FORMAT)) {
            System.out.println("Incorrect date format, see detailed usage");
            return null;
        }

        // make the API call
        String request = String.format(URL_BASE, from, to, dateFromStr, dateToStr);

        URL url;
        InputStreamReader reader;
        TravelResult tr;
        try {
            url = new URL(request);
            reader = new InputStreamReader(url.openStream());
            tr = new Gson().fromJson(reader, TravelResult.class);
            reader.close();
        } catch (IOException e) {
            System.out.println("Wrong parameters, see detailed usage");
            return null;
        }

        Data[] data = tr.getData();
        Data cheapestFlight = null;
        for (Data flight : data) {
            if (cheapestFlight == null || cheapestFlight.getPrice() > flight.getPrice()) {
                cheapestFlight = flight;
            }
        }

        cheapestFlight.setCurrency(tr.getCurrency());
        return cheapestFlight;
    }

    /**
     * Attempts to parse the date represented by a string.
     *
     * @param dateStr date represented by a string
     * @param sdf     simple date format used to parse the string
     * @return true if the string can be parsed
     */
    boolean isValidDateFormat(String dateStr, SimpleDateFormat sdf) {
        try {
            sdf.parse(dateStr);
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }

    private void printUsage() {
        System.out.println("Usage: departure location, arrival destination, date from, date to\n" +
                "Get detailed usage by entering: usage\n" +
                "Quit the application by entering: exit");
    }

    private void printDetailedUsage() {
        System.out.println("Usage: departure location (fly_from), arrival destination (fly_to), date from (date_from), date to (date_to)");
        System.out.println(USAGE);
    }

    private void run() {
        printUsage();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.equals("usage")) {
                printDetailedUsage();
            } else if (line.equals("exit")) {
                System.exit(0);
            } else if (line.contains(" ")) {
                System.out.println("No whitespaces are allowed");
            } else {
                try {
                    String split[] = line.split(",");
                    String from = split[0];
                    String to = split[1];
                    String dateFrom = split[2];
                    String dateTo = split[3];
                    Data result = getResult(from, to, dateFrom, dateTo);
                    if (result != null) {
                        System.out.println(result);
                    }
                } catch (ArrayIndexOutOfBoundsException ex) {
                    printUsage();
                }
            }
        }
    }

    public static void main(String[] args) {
        TravelController vc = new TravelController();
        vc.run();
    }

    private final String USAGE = "fly_from\n" +
            "    string (required) Example: CZ\n" +
            "    Kiwi api ID of the departure location.\n" +
            "    It accepts multiple values separated by comma, these values might be airport codes, city IDs, two letter country codes, metropolitan codes and radiuses as well as subdivision, region, autonomous_territory, continent and specials (Points of interest, such as Times Square).\n" +
            "    Some locations have the same code for airport and metropolis (city), e.g. DUS stands for metro code Duesseldorf, Moenchengladbach and Weeze as well as Duesseldorf airport. See the following examples:\n" +
            "        fly_from=city:DUS will match all airports in “DUS”, “MGL” and “NRN” (all in the city of Duesseldorf)\n" +
            "        fly_from=DUS will do the same as the above\n" +
            "        fly_from=airport:DUS will only match airport “DUS” Radius needs to be in form lat-lon-xkm. The number of decimal places for radius is limited to 6. E.g.-23.24--47.86-500km for places around Sao Paulo. ‘LON’ - checks every airport in London, ‘LHR’ - checks flights from London Heathrow, ‘UK’ - flights from the United Kingdom. Link to Locations API. Previous name: flyFrom\n" +
            "fly_to\n" +
            "    string (optional) Example: porto\n" +
            "    Kiwi api ID of the arrival destination. It accepts the same values in the same format as the fly_from parameter. Previous name: to.\n" +
            "    If you don’t include any value you’ll get results for all airports in the world.\n" +
            "    integer (required) Example: 3\n" +
            "    geographical data API version. Preferred value: 3.\n" +
            "date_from\n" +
            "    string (required) Example: 08/08/2017\n" +
            "    search flights from this date (dd/mm/YYYY). Use parameters date_from and date_to as a date range for the flight departure. Previous name: dateFrom Parameters ‘date_from=01/05/2016’ and ‘date_to=30/05/2016’ mean that the departure can be anytime between the specified dates. For the dates of the return flights, use the ‘return_to’ and ‘return_from’ or ‘nights_in_dst_from’ and ‘nights_in_dst_to’ parameters.\n" +
            "date_to\n" +
            "    string (required) Example: 08/09/2017\n" +
            "    search flights upto this date (dd/mm/YYYY). Previous name: dateTo\n";
}
